\documentclass[xetex]{beamer}

\title[Omnibus-gitlab]{Packaging GitLab using omnibus-ruby}
\subtitle{Taking installation instructions from 400 lines to 3 lines}
\author{Jacob Vosmaer}
\institute{GitLab B.V.}
\date[DevOps Amsterdam]
{DevOps Amsterdam meetup, 30 September 2014}

\begin{document}

\frame{\titlepage}

\begin{frame}
\tableofcontents
\end{frame}

\section{Introduction}

\begin{frame}
\frametitle{GitLab and GitLab B.V.}
\begin{itemize}[<+->]
  \item \href{https://about.gitlab.com/gitlab-ce/}{\textbf{GitLab}} is open source software to collaborate on code.
\item MIT open source license, 10k+ commits and 500+ contributors, lead author
  is Dmitriy Zaporozhets (UKR)
\item Used by individuals, small companies, government agencies and
  billion-dollar corporations.
\item \href{https://about.gitlab.com/}{\textbf{GitLab B.V.}} was co-founded by Sytse Sijbrandij (NL) and Dmitriy
\item GitLab B.V. offers GitLab Enterprise Edition, support subscriptions and
  consulting services.
\item We also offer a free GitLab SaaS at \url{https://gitlab.com}
\item My name is Jacob Vosmaer and I am a Senior Service Engineer at GitLab
  B.V.
\item The views I present here are my own and may or may not reflect those of my employer.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Getting to know you}
Who has ever \ldots
\begin{itemize}[<+->]
\item installed software on Linux?
\item installed software from source?
\item installed a Ruby on Rails (or similar) application from source?
\item shipped software for others (users) to install?
\item created their own RPM or Debian packages?
\end{itemize}
\end{frame}

\section{The omnibus-gitlab story}

\subsection{Challenge: GitLab was hard to install and maintain}
\begin{frame}
\frametitle{Challenge: GitLab was hard to install and maintain}
Installing GitLab in January 2014.
\begin{itemize}[<+->]
\item Install dozens of packages (Debian build-essential etc.)
\item Compile Ruby and Git from source
\item Orchestrate the integration between five daemons
\item Install an init script
\item Official documentation was only for Ubuntu 12.04
\end{itemize}

\pause

To make things worse, falling behind on the monthly GitLab release schedule
could quickly get painful for administrators.
\end{frame}

\subsection{Solution: imitate Chef Server}

\begin{frame}
\frametitle{Looking for solutions}
\begin{itemize}[<+->]
\item 'Native' Ubuntu packages seemed problematic for dependencies (we were
already compiling Ruby ourselves) and hard
\item Docker: too immature; installing GitLab should not require a back-ported
kernel and preferably no EPEL either
\item Omnibus-ruby: proven for an app of similar complexity (Chef Server) and
uses familiar tools (Ruby, Chef)
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{What is omnibus-ruby}
\begin{itemize}[<+->]
  \item \url{https://github.com/opscode/omnibus}
\item Packaging tool created by Chef, Inc. to distribute the Chef Client and Chef Server
  applications
\item Monolithic platform-specific packages with all dependencies compiled from
  source at build time
\item Chef Server uses Runit and Chef Solo to manage and configure the server
  application after it is installed
\item Open source, Apache license

\end{itemize}
\end{frame}

\begin{frame}
\frametitle{From omnibus-chef-server to omnibus-gitlab: a success!}

Based on the technology overlap between Chef Server and GitLab and our
familiarity with Ruby and Chef we decided to adapt
\href{https://github.com/opscode/omnibus-chef-server}{\textbf{omnibus-chef-server}},
the 'build lab' for Chef Server, into
\href{https://gitlab.com/gitlab-org/omnibus-gitlab}{\textbf{omnibus-gitlab}}.

\begin{itemize}[<+->]
\item
First pre-release of omnibus-gitlab on 14 February 2014 after 257 commits of
copy \& paste, search \& replace and head scratching.

\item
Users could now install and configure GitLab in only three steps: download,
install and reconfigure.

\item
We went from 400 lines of installation instructions to 3 lines.
\end{itemize}
\end{frame}

\subsection{Unexpected benefits}
\begin{frame}
\frametitle{Unexpected benefits}

Although we knew nothing about packaging, we got to copy the design of people
who did. Thank you Chef!

\begin{itemize}[<+->]
\item Supporting new distributions became MUCH easier: Centos 6 support took
  \texttt{3 files changed, 16 insertions(+), 5 deletions(-)}
\item Improved security through more restrictive file and directory permissions
\item A directory structure organized for DRBD-based High Availability
\item We now run our gitlab.com SaaS with omnibus packages
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Disadvantages}
\begin{itemize}[<+->]
\item Compared to a GitLab installation from source, users have less control.
  We are still adding configuration options where needed based on the demands
  of our users and ourselves (gitlab.com SaaS).
\item GitLab, as a software project, gained $\sim 5000$ lines of code.
\item The monthly GitLab releases take more effort now.
\end{itemize}
\end{frame}

\section{Technical topics}

\begin{frame}
\tableofcontents[currentsection]
\end{frame}

\subsection{Dependencies: to bundle or not to bundle}
\begin{frame}
\frametitle{Dependencies: to bundle or not to bundle}
\begin{itemize}[<+->]
\item Traditional Debian/RedHat packages use dependent packages to avoid
  installing software twice
\item Omnibus 'monolithic' packages avoid dependent packages and bundle their
  dependencies
\item Traditional packages are space-efficient and allow for 'global' security
  updates: fix OpenSSL once for the entire server
\item Disadvantages include lowest common denominator versions and inconsistent
  dependency versions across Linux distributions and OS versions
\item Monolithic packages make life easier for developers
\item When done right, there is a lower risk of breaking monoliths with
  \texttt{apt-get upgrade}
\item Omnibus-gitlab installs 900MB of files
\item A security release for a bundled dependency means a security release for
  the monolith
\end{itemize}
\end{frame}

\subsection{Omnibus-gitlab architecture}
\begin{frame}
\frametitle{Omnibus-gitlab architecture}
\begin{itemize}[<+->]
  \item
    \href{https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/config/projects/gitlab.rb}{\textbf{Building}}:
    Use omnibus-ruby to download, compile and install GitLab and all its
    dependencies on a build machine matching the deployment platform
  \item
    \href{https://gitlab.com/gitlab-org/omnibus-gitlab/tree/master/files/gitlab-cookbooks/gitlab}{\textbf{Configuring}}:
    Use chef-solo to create users, write dependency configuration files, create
    data directories etc.
  \item \href{http://smarden.org/runit/}{\textbf{Managing}}: Use Runit to
    start, stop and restart daemon processes.
  \item Configuration and management is done via
    \href{https://github.com/opscode/omnibus-ctl}{\textbf{omnibus-ctl}}, a
    combined front-end for chef-solo and Runit.
\end{itemize}

\end{frame}

\subsection{Consistent service management with Runit}
\begin{frame}
\frametitle{Consistent service management with Runit}

\begin{itemize}[<+->]

  \item \href{http://smarden.org/runit/}{\textbf{Runit}} is `a UNIX init scheme
    with service supervision'
  \item 'supervision' means automatic restarts of services
  \item Services are arranged in a supervision tree using parent-child process
    relations
  \item Simple, powerful and reliable
  \item The documentation is good but terse (there are only a few pages but you
    have to read them ten times)
  \item Omnibus-gitlab installs its Runit grandparent process via
    Upstart/Systemd/SysV init; Runit handles the actual omnibus-gitlab daemons.
  \item This is a big win: we get consistent service management across all
    Linux distributions we support with omnibus-gitlab
  \item Another piece of good design that Omnibus-gitlab inherited from Chef
    Server

\end{itemize}

\end{frame}

\subsection{Designing for DRBD}
\begin{frame}
\frametitle{Designing for DRBD}

\begin{itemize}[<+->]

  \item After working on omnibus-gitlab I was assigned to adapt our existing
    Chef cookbook to create a DRBD-based Highly Available GitLab deployment 
  \item Lots of cursing ensued as I tried to get all stateful services to write
    their data on the DRBD-replicated filesystem
  \item Then I realized this problem had already been solved by Omnibus-gitlab:
    all application state was being written in directories under
    \texttt{/var/opt/gitlab}, so all we had to do was mount a filesystem there
    and replicate it
  \item Another piece of good design that Omnibus-gitlab inherited from Chef
    Server

\end{itemize}

\end{frame}

\subsection{Omnibus-gitlab and security}
\begin{frame}
\frametitle{Omnibus-gitlab and security}

\begin{itemize}[<+->]

  \item Thanks to Chef Inc.\ promptly updating their OpenSSL build script we
    had
    \href{https://about.gitlab.com/2014/04/08/omnibus-packages-patched-against-cve-2014-0160/}{\textbf{new
    Omnibus-gitlab packages}} within 24 hours of the announcement of Heartbleed
    (OpenSSL CVE-2014-0160).
  \item In the past 8 months we had one issue where the design we copied from
    Chef Server was insecure. When we became aware of the problem we contacted
    Chef and did a
    \href{https://about.gitlab.com/2014/06/19/omnibus-gitlab-postgres-security-release/}{\textbf{joint}}
    security
    \href{https://www.getchef.com/blog/2014/06/26/security-vulnerability-releases-of-chef-server/}{\textbf{release}}
    to address this.
  \item One security issue so far in omnibus-ruby, the build tool that creates
    the actual .deb and .rpm packages. Chef fixed the issue within days of our
    report with a
    \href{https://www.getchef.com/blog/2014/09/19/security-releases-omnibus-2-0-2-and-3-2-2-insecure-file-ownership-in-omnibus-built-debian-and-ubuntu-packages/}{\textbf{security
    release}}.
  \item All in all, imitating Chef Server has raised the bar for security of
    GitLab deployments

\end{itemize}

\end{frame}

\begin{frame}
  Thank you for listening!
\end{frame}

\end{document}
