all: omnibus-gitlab.pdf omnibus-gitlab-handout.pdf

omnibus-gitlab.pdf: omnibus-gitlab.tex
	xelatex omnibus-gitlab
	xelatex omnibus-gitlab

omnibus-gitlab-handout.pdf: omnibus-gitlab.tex
	sed '1s/xetex/xetex,handout/' omnibus-gitlab.tex > omnibus-gitlab-handout.tex
	xelatex omnibus-gitlab-handout
	xelatex omnibus-gitlab-handout
